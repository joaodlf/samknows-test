package templates

// GetOutputTemplate returns the template for the output string.
func GetOutputTemplate(hasUnderPerforming bool) string {
	var template string = `SamKnows Metric Analyser v1.0.0
===============================

Period checked:

    From: {{.fromDate}}
    To:   {{.toDate}}

Statistics:

    Unit: Megabits per second

    Average: {{.avg}}
    Min: {{.min}}
    Max: {{.max}}
    Median: {{.median}}`

	if hasUnderPerforming {
		templateUnderPerforming := template + `

Under-performing periods:

    * The period between {{.underPerformStart}} and {{.underPerformEnd}}
      was under-performing.`

		return templateUnderPerforming
	}

	return template
}
