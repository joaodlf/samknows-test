package templates

import (
	"strings"
	"testing"
)

func TestGetOutputTemplateDefault(t *testing.T) {
	got := GetOutputTemplate(false)

	if strings.Contains(got, "Under-performing periods") {
		t.Errorf("GetOutputTemplate returned incorrect data")
	}
}

func TestGetOutputTemplateWithHasUnderPerforming(t *testing.T) {
	got := GetOutputTemplate(true)

	if !strings.Contains(got, "Under-performing periods") {
		t.Errorf("GetOutputTemplate returned incorrect data")
	}
}
