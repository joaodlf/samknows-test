package parse

import (
	"strings"
	"testing"

	"gitlab.com/joaodlf/samknows-test/internal/metrics"
)

func TestRun(t *testing.T) {
	got := Run(metrics.Metrics{struct {
		MetricValue float64 "json:\"metricValue\""
		DTime       string  "json:\"dtime\""
	}{12693166.98, "2018-01-29"}})

	if !strings.Contains(got, "SamKnows Metric Analyser") {
		t.Errorf("Run was incorrect, missing output")
	}
}

func TestRunWithUnderPerform(t *testing.T) {
	got := Run(metrics.Metrics{struct {
		MetricValue float64 "json:\"metricValue\""
		DTime       string  "json:\"dtime\""
	}{100000, "2018-01-29"}, {100000, "2018-01-30"}, {20000, "2018-01-31"}, {100000, "2018-02-01"}})

	if !strings.Contains(got, "Average: 0.64") {
		t.Errorf("Run was incorrect, missing output")
	}

	if !strings.Contains(got, "The period between 2018-01-31 and 2018-01-31") {
		t.Errorf("Run was incorrect, missing output")
	}
}
