package parse

import (
	"log"
	"time"

	"gitlab.com/joaodlf/samknows-test/internal/metrics"
	"gitlab.com/joaodlf/samknows-test/internal/output"
	"gitlab.com/joaodlf/samknows-test/pkg/format"
	"gitlab.com/joaodlf/samknows-test/pkg/maths"
)

// Run parses the data, calculates all of the metrics and returns the output.
func Run(m metrics.Metrics) string {
	// The expected date format.
	dtLayout := "2006-01-02"

	// The first and last dates (I don't assume asc order of the data).
	var dtFirst time.Time
	var dtLast time.Time

	// Same for the actual metric values.
	var min = m[0].MetricValue
	var max = m[0].MetricValue

	// Holds all the metric values so that we can do various operations.
	var metricValues []float64

	// A map representation of the data.
	metricMap := make(map[time.Time]float64)

	for i := range m {
		dtTime := m[i].DTime
		metricValue := m[i].MetricValue

		if min > metricValue {
			min = metricValue
		}

		if max < metricValue {
			max = metricValue
		}

		t, err := time.Parse(dtLayout, dtTime)

		if err != nil {
			log.Fatalf(err.Error())
		}

		if dtFirst.IsZero() || t.Before(dtFirst) {
			dtFirst = t
		}

		if dtLast.IsZero() || t.After(dtLast) {
			dtLast = t
		}

		metricMap[t] = metricValue
		metricValues = append(metricValues, metricValue)
	}

	// Standard Deviation.
	sd := maths.Sd(metricValues)
	// Average.
	avg := maths.Average(metricValues)
	// Lower Standard Deviation.
	sdLow := avg - sd
	// Coefficient of Variation.
	cv := (sd / avg) * 100

	// Determine if there are Under-performing periods.
	var underPerformStart time.Time
	var underPerformEnd time.Time
	if cv >= 1 {
		// We only want to consider Under-performing periods for a valid measure of dispersion.
		for date, metric := range metricMap {
			// Go does not iterate in order, so we have to do an array key map AND then iterate.
			// Otherwise, we can just compare values and make a decision before assignment. I opted for this.
			if metric < sdLow {
				if underPerformStart.IsZero() || date.Before(underPerformStart) {
					underPerformStart = date
				}

				if underPerformEnd.IsZero() || date.After(underPerformEnd) {
					underPerformEnd = date
				}
			}
		}
	}

	data := map[string]string{
		"fromDate": dtFirst.Format(dtLayout),
		"toDate":   dtLast.Format(dtLayout),
		"avg":      format.Number(maths.BytesToMegabits(maths.Average(metricValues))),
		"min":      format.Number(maths.BytesToMegabits(min)),
		"max":      format.Number(maths.BytesToMegabits(max)),
		"median":   format.Number(maths.BytesToMegabits(maths.Median(metricValues))),
	}

	if !underPerformStart.IsZero() && !underPerformEnd.IsZero() {
		data["underPerformStart"] = underPerformStart.Format(dtLayout)
		data["underPerformEnd"] = underPerformEnd.Format(dtLayout)
	}

	return output.String(data)
}
