package metrics

import (
	"bytes"
	"testing"
)

func TestFromFile(t *testing.T) {
	data := []byte(`[
		{
		  "metricValue": 12693166.98,
		  "dtime": "2018-01-29"
		},
		{
		  "metricValue": 12668239.57,
		  "dtime": "2018-01-30"
		},
		{
		  "metricValue": 12723772.1,
		  "dtime": "2018-01-31"
		}]`)

	reader := bytes.NewBuffer(data)

	_, err := FromFile(reader)

	if err != nil {
		t.Errorf(err.Error())
	}
}
