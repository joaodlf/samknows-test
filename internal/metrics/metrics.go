package metrics

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
)

// Metrics represents the expected JSON structure of the input files.
type Metrics []struct {
	MetricValue float64 `json:"metricValue"`
	DTime       string  `json:"dtime"`
}

// FromFile reads from a file.
func FromFile(file io.ReadWriter) (Metrics, error) {
	var metrics Metrics

	fileBytes, err := ioutil.ReadAll(file)

	if err != nil {
		log.Fatalf(err.Error())
	}

	err = json.Unmarshal(fileBytes, &metrics)

	return metrics, err
}
