package output

import (
	"bytes"
	"strings"
	"testing"
)

func TestString(t *testing.T) {
	got := String(map[string]string{
		"fromDate": "2018-01-29",
		"toDate":   "2018-02-27",
		"avg":      "102.7",
		"min":      "101.25",
		"max":      "104.08",
		"median":   "102.93",
	})

	if !strings.Contains(got, "2018-01-29") {
		t.Errorf("String was incorrect, missing fromDate")
	}

	if !strings.Contains(got, "2018-02-27") {
		t.Errorf("String was incorrect, missing fromDate")
	}

	if !strings.Contains(got, "102.7") {
		t.Errorf("String was incorrect, missing avg")
	}

	if !strings.Contains(got, "101.25") {
		t.Errorf("String was incorrect, missing min")
	}

	if !strings.Contains(got, "104.08") {
		t.Errorf("String was incorrect, missing max")
	}

	if !strings.Contains(got, "102.93") {
		t.Errorf("String was incorrect, missing median")
	}
}

func TestStringUnderPerform(t *testing.T) {
	got := String(map[string]string{
		"fromDate":          "2018-01-29",
		"toDate":            "2018-02-27",
		"avg":               "102.7",
		"min":               "101.25",
		"max":               "104.08",
		"median":            "102.93",
		"underPerformStart": "2018-02-05",
		"underPerformEnd":   "2018-02-07",
	})

	if !strings.Contains(got, "2018-01-29") {
		t.Errorf("String was incorrect, missing fromDate")
	}

	if !strings.Contains(got, "2018-02-27") {
		t.Errorf("String was incorrect, missing fromDate")
	}

	if !strings.Contains(got, "102.7") {
		t.Errorf("String was incorrect, missing avg")
	}

	if !strings.Contains(got, "101.25") {
		t.Errorf("String was incorrect, missing min")
	}

	if !strings.Contains(got, "104.08") {
		t.Errorf("String was incorrect, missing max")
	}

	if !strings.Contains(got, "102.93") {
		t.Errorf("String was incorrect, missing median")
	}

	if !strings.Contains(got, "2018-02-05") {
		t.Errorf("String was incorrect, missing underPerformStart")
	}

	if !strings.Contains(got, "2018-02-07") {
		t.Errorf("String was incorrect, missing underPerformEnd")
	}
}

func TesWrite(t *testing.T) {
	var output bytes.Buffer
	Write(&output, "test")

	expected := "test\n"
	got := output.String()

	if got != expected {
		t.Errorf("ToScreen was incorrect, got: %s, want: %s.", got, expected)
	}
}
