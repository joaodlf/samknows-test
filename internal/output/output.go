package output

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"text/template"

	"gitlab.com/joaodlf/samknows-test/templates"
)

//String returns the output of the template+data.
func String(data map[string]string) string {
	newTemplate := template.New("template")

	var t *template.Template
	var err error

	if _, ok := data["underPerformStart"]; ok {
		t, err = newTemplate.Parse(templates.GetOutputTemplate(true))
	} else {
		t, err = newTemplate.Parse(templates.GetOutputTemplate(false))
	}

	if err != nil {
		log.Fatalf(err.Error())
	}

	// Send data to buffer. Could just output here, but this makes testing much easier.
	var b bytes.Buffer
	if err := t.Execute(&b, data); err != nil {
		log.Fatalf(err.Error())
	}

	return b.String()
}

// Write sends the output to the screen or the file.
// Since io.Writer is an interface that works for both stdout and files, this makes it very easy to test.
func Write(w io.Writer, output string) {
	fmt.Fprint(w, output+"\n")
}
