# SamKnows

This is my proposed solution to the SamKnows test. It is written in Go 1.18.

The executable defaults to reading `inputs/1.json`, but a `-file` flag can also be provided:
```
samknows -file=inputs/2.json
```

Output defaults to screen. An `-output` flag is also available:
```
samknows -file=inputs/2.json -output file
```
(output is written to ./outputs)

Full documentation:
```
samknows -help
```

## Docker

To run the app within a container:
```
docker build --tag samknows-joao .
docker run -it --rm --name samknows-joao samknows-joao

root@b3bb3932dba4:/usr/src/app# samknows
```

## Building

To build the project manually:
```
go build -o samknows cmd/samknows/main.go
./samknows
```

You can also run the app without building:
```
go run cmd/samknows/main.go
```

## Tests

```
go test ./...
go test -cover ./...
```

## TODO

Further improvements can be made:
- The `parse.Run()` function could be split further. Iteration, calculations & output could be split into individual functions. I can also imagine creating a struct that holds all of the pertinent data and pass that struct around through the various functions.
- Flag parsing and validation could be moved away from `init()`, further validation and tests could be written as well.
- Further improvements to the overall project structure.
- I made the assumption that under-performing date ranges were calculated via the standard deviation + coefficient of variation. I am unsure if this is correct.
- Make use of a proper statistics third party package (such as https://pkg.go.dev/github.com/montanaflynn/stats), this would more than likely ensure the maths are correct.