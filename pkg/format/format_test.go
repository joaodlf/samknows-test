package format

import (
	"testing"
)

func TestNumber(t *testing.T) {
	expected := "95.51"
	got := Number(float64(95.5120))

	if got != expected {
		t.Errorf("FormatNumberOutput was incorrect, got: %s, want: %s.", got, expected)
	}
}

func TestNumberLeadingZero(t *testing.T) {
	expected := "95.5"
	got := Number(float64(95.5000))

	if got != expected {
		t.Errorf("FormatNumberOutput was incorrect, got: %s, want: %s.", got, expected)
	}
}
