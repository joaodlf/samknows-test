package format

import (
	"fmt"
	"strings"
)

// Number formats a given number to a format matching the output files.
func Number(number float64) string {
	return strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", number), "0"), ".")
}
