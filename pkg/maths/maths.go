package maths

import (
	"math"

	"golang.org/x/exp/constraints"
	"golang.org/x/exp/slices"
)

// Number is an interface that represents ALL number types.
type Number interface {
	constraints.Float | constraints.Integer
}

// BytesToMegabits converts Bytes (represented as a float) to Megabits.
func BytesToMegabits(b float64) float64 {
	return float64(b / 125000)
}

// Median calculates the median of a given data array.
func Median[T Number](data []T) float64 {
	dataCopy := make([]T, len(data))
	copy(dataCopy, data)

	slices.Sort(dataCopy)

	var median float64
	l := len(dataCopy)
	if l == 0 {
		return 0
	} else if l%2 == 0 {
		median = float64((dataCopy[l/2-1] + dataCopy[l/2]) / 2.0)
	} else {
		median = float64(dataCopy[l/2])
	}

	return median
}

// Average calculates the average of a given data array.
func Average[T Number](data []T) float64 {
	if len(data) == 0 {
		return 0
	}
	var sum float64
	for _, d := range data {
		sum += float64(d)
	}
	return sum / float64(len(data))
}

// Sd calculates the Standard Deviation of a given data array.
func Sd[T Number](data []T) float64 {
	var sd float64

	avg := Average(data)

	for _, d := range data {
		sd += math.Pow(float64(d)-avg, 2)
	}

	sd = math.Sqrt(sd / float64(len(data)))

	return sd
}
