package maths

import (
	"testing"
)

func TestBytesToMegabits(t *testing.T) {
	expected := float64(8)
	got := BytesToMegabits(float64(1000000))

	if got != expected {
		t.Errorf("BytesToMegabits was incorrect, got: %f, want: %f.", got, expected)
	}
}

func TestMedian(t *testing.T) {
	expected := float64(4.000000)
	got := Median([]float64{1, 2, 3, 4, 5, 6, 7.7})

	if got != expected {
		t.Errorf("TestMedian was incorrect, got: %f, want: %f.", got, expected)
	}
}

func TestAverage(t *testing.T) {
	expected := float64(4.100000)
	got := Average([]float64{1, 2, 3, 4, 5, 6, 7.7})

	if got != expected {
		t.Errorf("TestAverage was incorrect, got: %f, want: %f.", got, expected)
	}
}

func TestAverageEmpty(t *testing.T) {
	expected := float64(0)
	got := Average([]float64{})

	if got != expected {
		t.Errorf("TestAverage was incorrect, got: %f, want: %f.", got, expected)
	}
}

func TestSd(t *testing.T) {
	expected := float64(2.000000)
	got := Sd([]float64{1, 2, 3, 4, 5, 6, 7})

	if got != expected {
		t.Errorf("TestAverage was incorrect, got: %f, want: %f.", got, expected)
	}
}
