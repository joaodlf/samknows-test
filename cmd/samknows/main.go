package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/joaodlf/samknows-test/internal/metrics"
	"gitlab.com/joaodlf/samknows-test/internal/output"
	"gitlab.com/joaodlf/samknows-test/internal/parse"
)

var filePtr = flag.String("file", "inputs/1.json", "the input file")
var outputPtr = flag.String("output", "screen", "where to send output (screen|file). file will write to ./outputs")

func init() {
	// Parse the flags and do basic validation.
	flag.Parse()

	validOutput := func(flag string) bool {
		switch flag {
		case
			"screen",
			"file":
			return true
		}
		return false
	}(*outputPtr)

	if !validOutput {
		log.Fatalf("invalid output flag")
	}
}

func main() {
	// Open the file.
	f, err := os.Open(*filePtr)

	if err != nil {
		log.Fatalf(err.Error())
	}

	defer f.Close()

	m, err := metrics.FromFile(f)

	if err != nil {
		log.Fatalf(err.Error())
	}

	// Parse the input and get the output.
	outputStr := parse.Run(m)

	if *outputPtr == "screen" {
		// Write to screen and exit.
		output.Write(os.Stdout, outputStr)
		return
	}

	// Write to file.
	fileName := strings.Replace(f.Name(), "inputs/", "", 1)
	fileName = strings.Replace(fileName, ".json", ".output", 1)

	path := "outputs/" + fileName
	newF, err := os.Create(path)

	if err != nil {
		log.Fatalf(err.Error())
	}

	output.Write(newF, outputStr)
	fmt.Println("Output has been written to " + newF.Name())
}
